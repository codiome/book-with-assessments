{Check It!|assessment}(code-output-compare-2626010320)
{Check It!|assessment}(code-output-compare-2418450586)
{Check It!|assessment}(code-output-compare-1133304042)
{Check It!|assessment}(code-output-compare-2598046674)
{Check It!|assessment}(test-1660889877)
{Check It!|assessment}(fill-in-the-blanks-1086588113)
{Check It!|assessment}(fill-in-the-blanks-3531848661)
{Check It!|assessment}(multiple-choice-1757953055)
{Check It!|assessment}(multiple-choice-3616008394)
{Submit Answer!|assessment}(free-text-3431573355)
{Submit Answer!|assessment}(free-text-3131900173)
Inline `code` as you see here. 

|||info
# Important

If you are referring to HTML elements like `<div>`, `<b>` etc., you **must** wrap these it back ticks or they will render as HTML.

Indented code

    // Some comments
    line 1 of code
    line 2 of code
    line 3 of code


Block code "fences"

```
Sample text here...
```

Syntax highlighting

``` js
var foo = function (bar) {
  return bar++;
};

console.log(foo(5));
```