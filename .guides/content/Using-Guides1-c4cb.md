{Check It!|assessment}(code-output-compare-1883332920)
{Check It!|assessment}(code-output-compare-3309306319)
{Check It!|assessment}(code-output-compare-1303511789)
{Check It!|assessment}(code-output-compare-819268128)
{Check It!|assessment}(test-3788076578)
{Check It!|assessment}(fill-in-the-blanks-1576400819)
{Check It!|assessment}(fill-in-the-blanks-615481987)
{Check It!|assessment}(multiple-choice-1584447082)
{Check It!|assessment}(multiple-choice-80276331)
{Submit Answer!|assessment}(free-text-3825456162)
{Submit Answer!|assessment}(free-text-2120336734)



![.guides/img/ffc](.guides/img/ffc.bmp)
![.guides/img/ffc](.guides/img/ffc.gif)
![.guides/img/ffc](.guides/img/ffc.jpg)
![.guides/img/ffc](.guides/img/ffc.png)

## Available style
The later sections in this Guide show you the available markdown commands for Guides.

## Starting Guides

Codio Guides has 2 modes :

1. The Guides Editor - Tools->Guides->Edit menu item
1. The Guides Player - Tools->Guides->Play menu item

If you have not yet started work on a Guide, simply select the Edit option and you can start immediately.

## Documentation
Documentation can be found at https://codio.com/docs/ide/tools/guides/

If you find any anomalies, and there will be some due to the many changes we are currently making to Guides, then please email ijobling@codio.com with details and we will get this updated quickly.

